<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpParser\Node\Expr\AssignOp\BitwiseAnd;
use Validator;
use App\Models\Brand;

class BrandController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function brandList()
    {
        $brandNames = Brand::all();
        return view('brand.brandList', compact('brandNames'));
    }

    public function inactiveBrandList()
    {
        $inactiveBrandNames = Brand::where('status', 'inactive')->get();
        return view('brand.inactiveBrandList', compact('inactiveBrandNames'));
    }

    public function activeBrandList()
    {
        $activeBrandNames = Brand::where('status', 'active')->get();
        return view('brand.activeBrandList', compact('activeBrandNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function addBrand()
    {
        return view('brand.addBrand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function addbrandProcess(Request $request)
    {
        //dd($request->all());
        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

            'brandName' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $inputBrandName = $request->input('brandName');
        $checkBrand = Brand::select('brand_name')->where('brand_name', $inputBrandName)->first();
        //dd($checkBrand);
        if (!empty($checkBrand)) {
            $updateBrand = Brand::where('brand_name', $inputBrandName)->Update([
                'brand_name' => $request->input('brandName'),
            ]);
        } else {
            $createBrand = Brand::create([
                'brand_name' => $request->input('brandName'),
            ]);
        }
        //dd($test);
        session()->flash('message', 'Brand Added successfully.');
        return redirect()->route('brandList');
    }


    public function updateBand($id)
    {
        $brand_info = Brand::where('id', $id)->first();
        $brands = Brand::all();
        return view('brand.editBrand', compact('brand_info', 'brands'));


    }


    public function editBrandProcess(Request $request, $id)
    {
        Brand::where('id', $id)->update([
            'brand_name' => $request->input('brand_name')
        ]);

        $brandNames= Brand::all();
        session()->flash('message', 'Brand Updated Successfully');
        return view('brand.brandList',compact('brandNames'));


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function inactiveBrand($id)
    {
        $status = "inactive";
        $updateBrand = Brand::where('id', $id)->Update([
            'status' => $status,
        ]);

        session()->flash('message', 'Brand Inactive successfully.');
        return redirect()->back();
    }

    public function activeBrand($id)
    {
        $status = "active";
        $updateBrand = Brand::where('id', $id)->Update([
            'status' => $status,
        ]);

        session()->flash('message', 'Brand Inactive successfully.');
        return redirect()->back();
    }

}

?>
