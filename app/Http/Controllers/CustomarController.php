<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Customar;
use Validator;

class CustomarController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function customerList()
  {
    $cusDatas=Customar::all();
    return view('customer.customerList', compact('cusDatas'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function addCustomer()
  {
    return view('customer.addCustomer');
  }

  public function inactiveCustomerList()
  {
    $inactiveCusData=Customar::where('status', 'inactive')->get();
    return view('customer.inactiveCustomerList', compact('inactiveCusData'));
  }
  public function activeCustomerList()
  {
    $activeCusData=Customar::where('status', 'active')->get();
    return view('customer.activeCustomerList', compact('activeCusData'));
  }
  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function addCustomerProcess(Request $request)
  {
    //dd($request->all());
    $inputs = $request->except('_token');

    $validator = Validator::make($inputs, [

        'customerName' => 'required',
        'customerPhone' => 'required',
        'customerAddr' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    $inputCustomarPhone= $request->input('customerPhone');
    $checkCustomar=Customar::select('phone')->where('phone', $inputCustomarPhone)->first();
    //dd($checkBrand);
    if (!empty($checkCustomar))
    {
      session()->flash('message', 'Customer Exists In The Customer List.');
      return redirect()->back();
    }
    else
    {
      $createCustomar=Customar::create([
          'cus_name'=>$request->input('customerName'),
          'phone'=>$request->input('customerPhone'),
          'address'=>$request->input('customerAddr'),
      ]);
    }
    //dd($test);
    session()->flash('message', 'Customer Added successfully.');
    return redirect()->back();
  }

  public function updateCustomer($id)
  {
    $customer_info = Customar::where('id', $id)->first();
    $customers = Customar::all();
    return view('customer.editCustomer', compact('customer_info', 'customers'));


  }

  public function editCustomerProcess(Request $request,$id){


    Customar::where('id', $id)->update([
        'cus_name' => $request->input('customer_name'),
        'phone'=> $request->input('phone'),
        'address'=> $request->input('address'),


    ]);

    $cusDatas= Customar::all();
    session()->flash('message', 'Customer Updated Successfully');
    return view('customer.customerList',compact('cusDatas'));
  }




  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */

  public function inactiveCustomer($id)
  {
    $status="inactive";
    $updateCustomar=Customar::where('id', $id)->Update([
        'status'=>$status,
    ]);

    session()->flash('message', 'Customar Inactive successfully.');
    return redirect()->back();
  }
  public function activeCustomer($id)
  {
    $status="active";
    $updateCustomar=Customar::where('id', $id)->Update([
        'status'=>$status,
    ]);

    session()->flash('message', 'Customar Inactive successfully.');
    return redirect()->back();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>
