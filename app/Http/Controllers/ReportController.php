<?php

namespace App\Http\Controllers;

use App\Models\Customar;
use App\Models\Sale;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function todayDaySell()
    {
        $todaySell=Sale::where('confirm', 2)->whereDate('updated_at', Carbon::today())->with('sellCustomar')->with('sellProduct.productCate')->with('sellProduct.productBrand')->get();
        //dd($todaySell);
        return view('reports.todayList', compact('todaySell'));
    }
    public function searchSell()
    {
        return view('reports.searchSell');
    }
    public function searchReportProcess(Request $request)
    {
        //dd($request->all());
        $firstDate=$request->firstDate;
        $lastDate=$request->lastDate;
        $searchSell=Sale::where('confirm', 2)->whereBetween('updated_at', [$firstDate, $lastDate])->get();
        //dd($searchSell);
        return view('reports.searchSellResult', compact('searchSell'));
    }
    public function searchCustomerReport()
    {
        $cusName=Customar::all();
        return view('reports.searchCustomerReport', compact('cusName'));
    }
    public function searchCustomerReportProcess(Request $request)
    {
        $cusName=Customar::all();
        $searchCustomer=Customar::where('id', $request->customerName)->get();
        //dd($searchCustomer);
        return view('reports.searchCustomerReportResult', compact('searchCustomer', 'cusName'));
    }
}
