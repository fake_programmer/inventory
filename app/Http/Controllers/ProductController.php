<?php

namespace App\Http\Controllers;

use App\Models\ProductHistory;
use Illuminate\Http\Request;
use Validator;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }


    public function addProduct()
    {
        $bandName = Brand::all();
        $cateName = Category::all();

        return view('product.addProduct', compact('bandName', 'cateName'));
    }

    public function addProductProcess(Request $request)
    {
        //dd($request->all());

        $inputs = $request->except('_token');


        $validator = Validator::make($inputs, [

            'productName' => 'required',
            'brandName' => 'required',
            'categoryName' => 'required',
            'quantity' => 'required'

        ]);

        //$test=$request->productName;
        $productDetails=Product::where('cate_id', $request->categoryName)->where('brand_id', $request->bandName)->where('product_name', $request->productName)->first();
        //$test1=$productDetails->id;
        //dd($test1);
        if(!empty($productDetails)) {
            if ($productDetails->price==$request->price)
            {
                $updateCategory=Product::where('id', $productDetails->id)->Update([
                    'qty'=>$productDetails->qty+$request->input('quantity'),
                    'total_qty'=>$productDetails->total_qty+$request->input('quantity'),
                ]);
                session()->flash('message', 'Product Quantity Update successfully.');
                return redirect()->back();
            }else
            {
                $updateCategory=Product::where('id', $productDetails->id)->Update([
                    'qty'=>$request->input('quantity'),
                    'price'=>$request->input('price'),
                    'total_qty'=>$request->input('quantity'),
                    'created_at'=>now(),
                ]);
                $createProductHistory = ProductHistory::create([
                    'cate_id'=>$productDetails->cate_id,
                    'brand_id'=>$productDetails->brand_id,
                    'product_name'=>$productDetails->id,
                    'qty'=>$productDetails->qty,
                    'price'=>$productDetails->price,
                    'total_qty'=>$productDetails->total_qty,
                    'created_at'=>$productDetails->created_at,
                    'updated_at'=>now(),
                ]);
                session()->flash('message', 'Product New Price and Quantity Update successfully.');
                return redirect()->back();
            }
        }else{
            $createProduct = Product::create([
                'cate_id'=>$request->input('categoryName'),
                'brand_id'=>$request->input('bandName'),
                'product_name'=>$request->input('productName'),
                'qty'=>$request->input('quantity'),
                'price'=>$request->input('price'),
                'total_qty'=>$request->input('quantity'),
            ]);
            session()->flash('message', 'Product Added successfully.');
            return redirect()->back();
        }


        //$createProduct = Product::create([
        //    'product_name' => $request->input('productName'),
        //    'cate_id' => $request->input('categoryName'),
        //    'brand_id' => $request->input('bandName'),
        //    'qty' => $request->input('quantity'),
        //    'price' => $request->input('price')
        //
        //
        //]);
        //
        //
        //session()->flash('message', 'Product Added successfully.');
        //
        //return redirect()->route('productList');
    }


    public function productList()
    {
        $productNames = Product::all();
        return view('product.productList', compact('productNames'));


    }


    public function updateProduct($id){

        $product_info = Product::where('id', $id)->first();
        $products = Brand::all();
        return view('product.editProduct', compact('product_info', 'products'));
    }


    public function editProductProcess(Request $request, $id)
    {
        Product::where('id', $id)->update([
            'product_name' => $request->input('product_name'),
            'qty'=> $request->input('quantity'),
            'price'=>$request->input('price'),

        ]);

        $productNames= Product::all();
        session()->flash('message', 'Product Updated Successfully');
        return view('product.productList',compact('productNames'));


    }




    public function inactiveProduct($id)
    {
        $status="inactive";
        $updateProduct=Product::where('id', $id)->Update([
            'status'=>$status,
        ]);

        session()->flash('message', 'Product Inactive successfully.');
        return redirect()->back();


    }

    public function activeProduct($id)
    {
        $status="active";
        $updateProduct=Brand::where('id', $id)->Update([
            'status'=>$status,
        ]);

        session()->flash('message', 'Product Inactive successfully.');
        return redirect()->back();
    }


    public function inactiveProductList()
    {
        $inactiveProductNames=Product::where('status', 'inactive')->get();
        return view('product.inactiveProductList', compact('inactiveProductNames'));
    }
    public function activeProductList()
    {
        $activeProductNames=Product::where('status', 'active')->get();
        return view('product.activeProductList', compact('activeProductNames'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {

    }

}

?>
