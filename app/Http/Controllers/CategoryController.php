<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;

use Validator;
use Session;
use App\Models\Category;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function categoryList()
    {
        $categoryNames = Category::with('brandCategory')->get();
        return view('category.categoryList', compact('categoryNames'));
    }

    public function inactiveCategoryList()
    {
        $inactiveCategoryNames = Category::where('status', 'inactive')->with('brandCategory')->get();
        return view('category.inactiveCategoryList', compact('inactiveCategoryNames'));
    }

    public function activeCategoryList()
    {
        $activeCategoryNames = Category::where('status', 'active')->with('brandCategory')->get();
        return view('category.activeCategoryList', compact('activeCategoryNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function addCategory()
    {
        $brandName = Brand::all();
        return view('category.addCategory', compact('brandName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function addCategoryProcess(Request $request)
    {
        //dd($request->all());
        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

            'categoryName' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //$inputCategoryName= $request->categoryName;

        $checkCategory = Category::select('cate_name')->where('cate_name', $request->categoryName)->where('brand_id', $request->brandName)->first();
        //dd($checkCategory);
        if (!empty($checkCategory)) {
            session()->flash('message', 'Sorry !! This is Super Admin.');
            return redirect()->route('categoryList');
        } else {
            $createCategory = Category::create([
                'cate_name' => $request->input('categoryName'),
                'brand_id' => $request->input('brandName'),
            ]);
        }
        //dd($test);
        session()->flash('message', 'Category Added successfully.');
        return redirect()->route('categoryList');
    }


    public function updateCategory($id)
    {
        $category_info=Category::where('id', $id)->first();
        $category = Category::all();
        return view('category.editCategory', compact('category_info', 'category'));

    }

    public function editCategoryProcess(Request $request, $id)
    {
        Category::where('id', $id)->update([
            'cate_name' => $request->input('category_name')
        ]);

        $categoryNames= Category::all();
        session()->flash('message', 'Category Updated Successfully');
        return view('category.categoryList',compact('categoryNames'));


    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */

    public function inactiveCategory($id)
    {
        $status = "inactive";
        $updateCategory = Category::where('id', $id)->Update([
            'status' => $status,
        ]);

        session()->flash('message', 'Brand Inactive successfully.');
        return redirect()->back();
    }

    public function activeCategory($id)
    {
        $status = "active";
        $updateCategory = Category::where('id', $id)->Update([
            'status' => $status,
        ]);

        session()->flash('message', 'Brand Inactive successfully.');
        return redirect()->back();
    }

}

?>
