<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{

   protected $table = 'categorys';
//    public $timestamps = true;
//    protected $fillable = array('brand_id', 'cate_name', 'status');

    protected $guarded = [];
    public function brandCategory()
    {
        return $this->hasMany('App\Models\Brand', 'id', 'brand_id');
    }

}
