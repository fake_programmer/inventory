@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Inactive Brand List</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Brand Name
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($inactiveBrandNames as $inactiveBrandName)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$inactiveBrandName->brand_name}}
                                        </td>
                                        <td>
                                            {{$inactiveBrandName->status}}
                                        </td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                Edit
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title edit_modal_title" id="exampleModalLabel">Edit Brand</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="" action="{{route('editBrandProcess', $inactiveBrandName->id)}}" role="" method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="brandName">Brand Name</label>
                                                                    <input type="text" name="brandName" class="form-control" id="brandName" value="{{$inactiveBrandName->id}}">
                                                                </div>
                                                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                                                <button class="btn btn-danger" type="reset">Reset</button>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($inactiveBrandName->status=="active")
                                                <a href="{{route('inactiveBrand', $inactiveBrandName->id)}}" class="btn btn-info">
                                                    Inactive
                                                </a>
                                            @else
                                                <a href="{{route('activeBrand', $inactiveBrandName->id)}}" class="btn btn-info">
                                                    Active
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
