<div class="container-fluid page-body-wrapper">
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
            <li class="nav-item nav-profile">
                <div class="nav-link">
                    <div class="user-wrapper">
                        <div class="profile-image">
                            <img src="{{asset('img/1.png')}}" alt="profile image">
                        </div>
                        <div class="text-wrapper">
                            <p class="profile-name">Mr. Immam</p>
                            <div>
                                <small class="designation text-muted">Owner</small>
                                <span class="status-indicator online"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('index')}}">
                    <i class="menu-icon mdi mdi-television"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('menuItem')}}">
                    <i class="menu-icon mdi mdi-television"></i>
                    <span class="menu-title">Menu Item</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#brand" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon mdi mdi-restart"></i>
                    <span class="menu-title">Brand</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="brand">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('brandList')}}"> Brand List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('inactiveBrandList')}}">Inactive Brand List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('activeBrandList')}}">Active Brand List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('addBrand')}}">Add Brand </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#category" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon mdi mdi-restart"></i>
                    <span class="menu-title">Category</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="category">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('categoryList')}}"> Category List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('inactiveCategoryList')}}">Inactive Category List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('activeCategoryList')}}">Active Category List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('addCategory')}}">Add Category </a>
                        </li>
                    </ul>
                </div>
            </li>


            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon mdi mdi-restart"></i>
                    <span class="menu-title">Products</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="products">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('productList')}}"> Products List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('inactiveProductList')}}">Inactive Products List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('activeProductList')}}">Active Products List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('addProduct')}}">Add Product </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#customer" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon mdi mdi-restart"></i>
                    <span class="menu-title">Customer</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="customer">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('customerList')}}"> Customer List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('addCustomer')}}">Add Customer </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('inactiveCustomerList')}}">Inactive Customer List </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('activeCustomerList')}}">Active Customer List </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#sell" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon mdi mdi-restart"></i>
                    <span class="menu-title">Sell</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="sell">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('addSell')}}">Add Sell </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('confirmSell')}}">Confirm </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#report" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon mdi mdi-restart"></i>
                    <span class="menu-title">Reports</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="report">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('todayDaySell')}}">Today Sell </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('searchSell')}}">Search Sell Report </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('searchCustomerReport')}}">Search Customer Report </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </nav>
    <!-- partial -->
    <div class="main-panel">
