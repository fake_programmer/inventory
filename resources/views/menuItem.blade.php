@extends('master')
@section('sidebar')
    @include('partials.sidebar')
@stop
@section('contant')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <ul class="nav">
                        <li class="nav-item single_menu">
                            <a class="nav-link" href="{{route('index')}}">
                                <i class="menu-icon mdi mdi-television"></i>
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item single_menu">
                            <a class="nav-link" href="{{route('menuItem')}}">
                                <i class="menu-icon mdi mdi-television"></i>
                                <span class="menu-title">Menu Item</span>
                            </a>
                        </li>
                        <li class="nav-item single_menu">
                            <a class="nav-link" href="{{route('index')}}">
                                <i class="menu-icon mdi mdi-television"></i>
                                <span class="menu-title">Customar Report</span>
                            </a>
                        </li>
                        <li class="nav-item single_menu">
                            <a class="nav-link" href="{{route('menuItem')}}">
                                <i class="menu-icon mdi mdi-television"></i>
                                <span class="menu-title">Product List</span>
                            </a>
                        </li>
                        <li class="nav-item single_menu">
                            <a class="nav-link" href="{{route('menuItem')}}">
                                <i class="menu-icon mdi mdi-television"></i>
                                <span class="menu-title">Total SSSS</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Customer Details</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Customar Name
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Total Due
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allCustomer as $data)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$data->cus_name}}
                                        </td>
                                        <td>
                                            {{$data->phone}}
                                        </td>
                                        <td>
                                            {{$data->due}}
                                        </td>
                                        <td>

                                        <td>
                                            <a class="fontawesom_icon" href="{{route('confirmSellProcess', $data->id)}}">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop





