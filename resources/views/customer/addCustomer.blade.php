@extends('master')
@section('sidebar')
    @include('partials.sidebar')
@stop
@section('contant')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            @if(Session::has('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p>
                            @endif
                            <div class="card-body">
                                <h1 class="card-title">Add Customer</h1>
                                <form class="forms-sample" action="{{route('addCustomerProcess')}}" role="" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="customerName">Customer Name</label>
                                        <input type="text" name="customerName" class="form-control" id="customerName" placeholder="Enter Customer Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="customerPhone">Customer Phone</label>
                                        <input type="text" name="customerPhone" class="form-control" id="customerPhone" placeholder="Enter Phone Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="customerAddr">Customer Address</label>
                                        <input type="text" name="customerAddr" class="form-control" id="customerAddr" placeholder="Enter Address">
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
