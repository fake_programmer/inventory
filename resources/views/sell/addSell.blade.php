@extends('master')
@section('sidebar')
    @include('partials.sidebar')
@stop
@section('contant')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            @if(Session::has('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card-body">
                                <h1 class="card-title">Add Sell</h1>
                                <form class="forms-sample" action="{{route('addSellProcess')}}" role="" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="customerName">Customer Name</label>
                                        <select name="customerName" class="form-control" id="customerName">
                                            <option value="">--Select Customer--</option>
                                            @foreach($cusName as $cusData)
                                                <option value="{{$cusData->id}}">{{$cusData->cus_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="brandName">Brand Name</label>
                                        <select name="brandName" class="form-control" id="sellbrandName">
                                            <option value="">--Select Brand--</option>
                                            @foreach($brandName as $brandData)
                                                <option value="{{$brandData->id}}">{{$brandData->brand_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cateName">Category Name</label>
                                        <select name="cateName" class="form-control cateName" id="sellcateName">
                                            <option value="">--Select Brand--</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="productName">Product Name</label>
                                        <select name="productName" class="form-control productName" id="sellproductName">
                                            <option value="">--Select Category--</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" name="price" class="form-control" id="sellprice" value="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="sellQty">Quantity <input type="text" id="sellMaxQty" readonly style="border: none; width: 180px"></label>
                                        <input type="text" name="qty" class="form-control" id="sellQty" placeholder="Enter Quantity">
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Add New</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



