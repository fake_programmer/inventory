@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h1 class="card-title">Add Category</h1>
                                <form class="forms-sample" action="{{route('addCategoryProcess')}}" role="" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="categoryName">Category Name</label>
                                        <input type="text" name="categoryName" class="form-control" id="categoryName" placeholder="Enter Category Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="brandName">Brand Name</label>
                                        <select name="brandName" class="form-control" id="brandName">
                                            <option value="">--Select Brand--</option>
                                            @foreach($brandName as $brandNames)
                                                <option value="{{$brandNames->id}}">{{$brandNames->brand_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
