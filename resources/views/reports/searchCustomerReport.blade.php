@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Search Customer Report</h1>
                        <form class="forms-sample" action="{{route('searchCustomerReportProcess')}}" role="" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="customerName">Customer Name</label>
                                <select name="customerName" class="form-control" id="customerName">
                                    <option value="">--Select Customer--</option>
                                    @foreach($cusName as $cusData)
                                        <option value="{{$cusData->id}}">{{$cusData->cus_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Search</button>
                            <button class="btn btn-danger" type="reset">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
