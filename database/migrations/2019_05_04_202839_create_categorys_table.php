<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategorysTable extends Migration {

	public function up()
	{
		Schema::create('categorys', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('brand_id')->unsigned();
			$table->string('cate_name', 32);
			$table->string('status', 32)->default('active');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('categorys');
	}
}