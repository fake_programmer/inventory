<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrandsTable extends Migration {

	public function up()
	{
		Schema::create('brands', function(Blueprint $table) {
			$table->increments('id');
			$table->string('brand_name', 32);
			$table->string('status', 32)->default('active');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('brands');
	}
}